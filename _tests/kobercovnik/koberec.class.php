<?php
/* REQUIREMENTS: $user_info */

class Koberec {
	var $id, $cil, $id_vlastnik;
	
	function Koberec ($vstup) {
		global $user_info;
		
		if ($vstup == -1) return 0;

		/* pokud neni vstup ID v databazi, tak rozkladam vstup z hospodareni */
		if (!is_numeric ($vstup)) {
			$vstup = $this->novyKoberec ($vstup);
		}
		
		$this->nactiDB ($vstup);
	}
	
	/* nacte udaje o koberci z databaze (podle $id) */
	function nactiDB ($id) {
		
		
		if (!$data = MySQL_Fetch_Array (MySQL_Query ("SELECT `koberce`.*, `users`.`regent`, `users`.`login` FROM `koberce` 
						INNER JOIN `users`
						ON `users`.`ID` = `koberce`.`ID_vlastnik`
						WHERE `koberce`.`ID` = '{$id}'"))) return 0;
		
		$this->id = $data['ID'];
		/* info o cili */
		$this->cil['sila'] = $data['cil_sila'];
		$this->cil['ID'] = $data['cil'];
		$this->cil['regent'] = $data['cil_regent'];
		$this->cil['provincie'] = $data['cil_provincie'];
		$this->cil['povolani'] = $data['cil_povolani'];
		$this->cil['rasa'] = $data['cil_rasa'];
		$this->cil['pohlavi'] = $data['cil_pohlavi'];
		$this->cil['slava'] = $data['cil_slava'];
		$this->cil['lidi'] = $data['cil_lidi'];
		$this->cil['hrady'] = $data['cil_hrady'];
		$this->cil['zlato'] = $data['cil_zlato'];
		$this->cil['mana'] = $data['cil_mana'];
		$this->cil['rozloha'] = $data['cil_rozloha'];
		$this->cil['presvedceni'] = $data['cil_presvedceni'];
		$this->cil['aliance'] = $data['cil_aliance'];
		$this->cil['bounty1'] = $data['bounty1'];
		$this->cil['bounty2'] = $data['bounty2'];
		$this->cil['bounty3'] = $data['bounty3'];
		$this->cil['noob'] = $data['noob'];
		$this->cil['cska'] = $data['cska'];
		$this->cil['poznamka'] = $data['poznamka'];
		$this->cil['verejny'] = $data['verejny'];
		
		$this->id_vlastnik = $data['ID_vlastnik'];
	}
	
	/* vrati string - tabulku informaci o cili, pokud je 
	*	$uprav == 1, tak bude editovatelny (tzn. misto textu
	*	budou inputy (<- 2do) */
	function infoOCili ($uprav = 0) {
		$retval = '';
		$retval .= "
		<table class=\"o_cili\">
		<tr>
			<td>Regent</td><td>{$this->cil['regent']}</td>
		</tr>
		<tr>
			<td>Povol�n�</td><td>{$this->cil['povolani']}</td>
		</tr>
		<tr>
			<td>Rasa</td><td>{$this->cil['rasa']}</td>
		</tr>
		<tr>
			<td>Pohlav�</td><td>{$this->cil['pohlavi']}</td>
		</tr>
		<tr>
			<td>Sl�va</td><td>{$this->cil['slava']}</td>
		</tr>
		<tr>
			<td>Po�et lid�</td><td>{$this->cil['lidi']}</td>
		</tr>
		<tr>
			<td>Po�et hrad�</td><td>{$this->cil['hrady']}</td>
		</tr>
		<tr>
			<td>S�la P.</td><td>{$this->cil['sila']}</td>
		</tr>
		<tr>
			<td>Zlato</td><td>{$this->cil['zlato']}</td>
		</tr>
		<tr>
			<td>Mana</td><td>{$this->cil['mana']}</td>
		</tr>
		<tr>
			<td>Rozloha</td><td>{$this->cil['rozloha']}</td>
		</tr>
		<tr>
			<td>P�esv�d�en�</td><td>{$this->cil['presvedceni']}</td>
		</tr>
		<tr>
			<td>Aliance</td><td>{$this->cil['aliance']}</td>
		</tr>
		</table>";
		
		return $retval;
	}

	function toStr () {
		global $user_info;
		
		if (!$info = MySQL_Fetch_Array (MySQL_Query ("SELECT * FROM `users` WHERE `ID` = '{$this->id_vlastnik}'"))) return null;
		$retval = '<div class="koberec">
		<div class="pole">Vypsal: '.$info['regent'].' ('.$info['login'].') '.icq($info['icq']).'</div> <!-- /pole^vypsal -->
		<div class="pole"><strong><u>C�l:</u></strong><br>';

		$retval .= $this->infoOCili ();
		
		//.nl2br(htmlspecialchars($koberec['cil'])).
		
		$retval .= '</div> <!-- /pole^cil -->';
		
		if ($this->cil['cska']) $retval .= '<div class="pole"><strong><u>CSka:</u></strong><br>'.nl2br(htmlspecialchars($this->cil['cska'])).'</div> <!-- /pole^CSka -->';
		if ($this->cil['poznamka'])$retval .= '<div class="pole"><strong><u>Pozn�mka:</u></strong><br>'.nl2br(htmlspecialchars($this->cil['poznamka'])).'</div> <!-- /pole^pozn -->';
		for ($poradi = 1; $poradi <= 3; $poradi ++) {
		
			$ma_zapsany = MySQL_Num_Rows (MySQL_Query ("SELECT * FROM `koberce_users` WHERE `ID_koberce` = '".$this->id."' AND `ID_users` = '".$user_info['ID']."' AND `poradi` = '".$poradi."'"));
			
			$retval .= '<div class="pole">
				St�elec '.$poradi.($this->cil['bounty'.$poradi] > 0 ? " (<strong>".$this->cisloSRadem($this->cil['bounty'.$poradi])." zl</strong>)" : "").' pwr cca '.($this->silaStrelce($this->cil['sila'], $poradi, $this->cil['noob'])).':<br>
				<div class="sub">';
			if ($ma_zapsany) {
				$retval .= '<a href="main.php?akce=koberce&amp;k_akce=odepsat&amp;k_id='.$this->id.'&amp;k_poradi='.$poradi.'">Odepsat se (z '.$poradi.'. pozice)</a><br>
				';
			} else {
				$retval .= '
				<form action="main.php" method="post" class="zapis_tepichu">
					<input type="hidden" name="akce" value="koberce">
					<input type="hidden" name="k_id" value="'.$this->id.'">
					<input type="hidden" name="poradi" value="'.$poradi.'">
					<input type="submit" name="k_akce" value="zapis" class="submit"> (<input name="poznamka" value="">)					
				</form>';
				/*$retval .= '<a href="main.php?akce=koberce&amp;k_akce=zapsat&amp;k_id='.$this->id.'&amp;k_poradi='.$poradi.'">Zapsat se (jako '.$poradi.'ka)</a><br>
				';*/
			}
			$zapsani_db = MySQL_Query ("SELECT `users`.* , `koberce_users`.`poznamka`
												FROM `koberce_users` 
												INNER JOIN `users` ON `users`.`ID` = `koberce_users`.`ID_users` 
												WHERE `koberce_users`.`ID_koberce` = '".$this->id."' AND `koberce_users`.`poradi` = '$poradi'");
			while ($zapsani = MySQL_Fetch_Array ($zapsani_db)) {
				$retval .= '<div class="strelec">
				'.$zapsani['regent']." (ID ".$zapsani['login'].") ".icq($zapsani['icq']).'
				</div> <!-- //strelec -->';
				if ($zapsani['poznamka'])
					$retval .= '<div class="pozn">
						"'.htmlspecialchars($zapsani['poznamka']).'"
					</div> <!-- // -->';
			}
			
			$retval .= "</div> <!-- /sub^strelci {$zapsani['regent']} -->
			</div> <!-- /pole^strelci {$poradi} -->";
		}
		$retval .= '</div> <!-- /koberec -->';
		
		return $retval;
	}
	
	/* zapise uzivateli s ID = $u_id koberec + nastavi poznamku */
	function zapisStrelce ($u_id, $pozn, $poradi) {
		/* 1. kontrola jestli mam na koberec narok */
		if (!$this->maNarok ($u_id)) {
			echo "Nem�te pr�vo zapsat si tento koberec.<br>";
			return 0;
		}
		/* 2. kontrola jestli jsem se uz nezapsal */
		$test = MySQL_Num_Rows(MySQL_Query ("SELECT * FROM `koberce_users` WHERE `ID_users` = '$u_id' AND `ID_koberce` = '{$this->id}' AND `poradi` = '{$poradi}'"));
		if ($test > 0) {
			echo "U� tento koberec m�te zaps�n.<br>";
			return 0;
		}
		MySQL_Query ("INSERT INTO `koberce_users` ( `ID` , `ID_users` , `ID_koberce` , `poradi` , `poznamka` ) 
													VALUES ('' , '{$u_id}', '{$this->id}', '{$poradi}', '{$pozn}');");
	}
	
	function odepisStrelce ($u_id, $poradi) {
		if (MySQL_Query ("DELETE FROM `koberce_users` WHERE `ID_users` = '$u_id' AND `ID_koberce` = '{$this->id}' AND `poradi` = '$poradi'"))
			return 1;
		return 0;
	}
	
	
	/* odpovi, jestli ma dany uzivatel narok na tento tepich */
	function maNarok ($u_id) {
		// 2do
		return 1;
	}
	
	/* jakou ma silu strelec na urcity cil (pokud je cil n00b/profi a kolikaty strelec jde */
	function silaStrelce($pwr, $poradi, $noob)
	{
		if ($noob) 
			$sily = array (1 => 1.25, 1.25 * 0.7, 1.25 * 0.75 * 0.55);
		else
			$sily = array (1 => 1.25, 1.25 * 0.75, 1.25 * 0.75 * 0.625);
		if (!$sily[$poradi])
			return 0;
			
		return $this->cisloSRadem($pwr * $sily[$poradi]);
	}
	
	
	/* vypise upravovaci tabulku. Pokud $uprav == 1, tak i upravi v databazi */
	function uprav ($uprav = 0) {
		/* 1. pokud ho mam upravit, tak to musim udelat jako prvni krok (abych ho vypsal uz upraveny) */
		if ($uprav) {
			$query = "UPDATE `koberce` SET ";
			if ($match = $this->rozklad($_POST['k_cil'])) {
				$query .= "`cil` = '{$match[3]}',
							  `cil_povolani` = '{$match[4]}',
							  `cil_rasa` = '{$match[5]}',
							  `cil_pohlavi` = '{$match[6]}',
							  `cil_slava` = '{$match[7]}',
							  `cil_lidi` = '{$match[8]}',
							  `cil_hrady` = '{$match[9]}',
							  `cil_zlato` = '{$match[11]}',
							  `cil_mana` = '{$match[12]}',
							  `cil_rozloha` = '{$match[13]}',
							  `cil_aliance` = '{$match[14]}',
							  `cil_sila` = '{$match[10]}', ";
			}
			
			$query .= "`bounty1` = '{$_POST['k_bounty1']}',
						  `bounty2` = '{$_POST['k_bounty2']}',
						  `bounty3` = '{$_POST['k_bounty3']}',
						  `noob` = '{$_POST['k_noob']}',
						  `poznamka` = '{$_POST['k_pozn']}',
						  `cska` = '{$_POST['k_cska']}',
						  `verejny` = '".($_POST['k_verejny'] == 'on' ? '1' : '0')."'";

			$query .= " WHERE `ID` = '{$this->id}'";
			
			if (MySQL_Query ($query)) {
				$this->nactiDB($this->id);
			} else {
				echo '<div class="error">
					Chyba p�i upravov�n� koberce!
				</div> <!-- //error -->';
			}
		}
		echo '<div class="pravy">
				<form action="main.php" class="novy_koberec" method="post">
					<input type="hidden" name="akce" value="koberce">
					<input type="hidden" name="typ" value="sprava">
					<input type="hidden" name="s_id" value="'.$this->id.'">
					<table>
						<tr>
							<td>C�l:</td>
							<td>'."{$this->cil['regent']}, {$this->cil['provincie']} ({$this->cil['ID']})".'</td>
						</tr>
						<tr>
							<td>Text (�pehov�):</td>
							<td><textarea name="k_cil"></textarea></td>
						</tr>
						<tr>
							<td>CSka:</td>
							<td><textarea name="k_cska">'.htmlspecialchars($this->cil['cska']).'</textarea></td>
						</tr>
						<tr>
							<td>Poznamka:</td>
							<td><textarea name="k_pozn">'.htmlspecialchars($this->cil['poznamka']).'</textarea></td>
						</tr>
						<tr>
							<td>Ve�ejn�:</td>
							<td><input type="checkbox" name="k_verejny"'.($this->cil['verejny'] ? ' checked' : '').'></td>
						</tr>
						<tr>
							<td>Bounty pro 1ku:</td>
							<td><input name="k_bounty1" value="'.$this->cil['bounty1'].'"></td>
						</tr>
						<tr>
							<td>Bounty pro 2ku:</td>
							<td><input name="k_bounty2" value="'.$this->cil['bounty2'].'"></td>
						</tr>
						<tr>
							<td>Bounty pro 3ku:</td>
							<td><input name="k_bounty3" value="'.$this->cil['bounty3'].'"></td>
						</tr>
						<tr>
							<td>�rove�:</td>
							<td><select name="k_noob" size=2>
								<option value="1"'.($this->cil['noob'] == 1 ? " selected" : "").'>norm�ln� hr��</option>
								<option value="0"'.($this->cil['noob'] == 0 ? " selected" : "").'>n00b</option>
							</select></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" name="submit" value="Upravit"><br>
							<br>
							<input type="submit" name="submit" value="Smazat" onClick=\'return window.confirm("Opravdu chcete smazat?")\'></td>
						</tr>
					</table>
					
					</form>
				</div> <!-- //pravy -->';
	}
	
	/* $vstup je pole, musi obsahovat:
	*		'cil' - spehove na cil
	*		'ID' - ID vlastnika
	*		'CSka' - CSka
	*		'Poznamka' - poznamka
	*		'bounty1'...'bounty3' - odmeny
	*		'noob' - 1 = noob, 0 = hrac 
	*	navratova hodnota je ID vlozeneho koberce */
	function novyKoberec ($vstup) {
		if ($match = $this->rozklad($vstup['cil'])) {				
			if (MySQL_Query ("INSERT INTO `koberce` ( `ID` , `ID_vlastnik` , 	`cil` , 			`cska` , 					`poznamka` , 				`cil_regent` , `cil_provincie` , `cil_povolani` , `cil_rasa` , `cil_pohlavi` , `cil_slava` , `cil_lidi` , `cil_hrady` , `cil_zlato` , `cil_mana` , `cil_rozloha` , `cil_presvedceni` , `cil_aliance` , `cil_sila` , `bounty1` , `bounty2` , `bounty3` , `time` , `noob`, `verejny` ) 
																VALUES ('', '{$vstup['ID']}', '{$match[3]}', '{$vstup['CSka']}', '{$vstup['poznamka']}', '{$match[1]}', '{$match[2]}', '{$match[4]}', '{$match[5]}', '{$match[6]}', '{$match[7]}', '{$match[8]}', '{$match[9]}', '{$match[11]}', '{$match[12]}', '{$match[13]}', '{$match[14]}', '{$match[15]}', '{$match[10]}', '{$vstup['bounty1']}', '{$vstup['bounty2']}', '{$vstup['bounty3']}', '".time()."', '{$vstup['noob']}', '".($_POST['k_verejny'] == 'on' ? '1' : '0')."');") 
				&&	$id = MySQL_Fetch_Array (MySQL_Query ("SELECT `ID` FROM `koberce` ORDER BY `ID` DESC"))) {			
					echo "Koberec �sp�n� p�id�n.<br>";
					return $id[0];
			} else {
				echo "Chyba p�i vkl�d�n� koberce do datab�ze.<br>";
				return 0;
			}
		}	
		echo '<div class="error">
			Chyba p�i rozkladu dat!
		</div> <!-- //error -->';	
		return 0;
	}
	
	/* vrati rozlozene hospodareni jako pole, pokud 
	*	neprojde, vraci 0 */
	function rozklad ($vstup) {
		if (preg_match ('/
				regent\s+(.+?)\s*\r\n						#1 regent
				provincie\s+(.+?)\s+\((\d+)\)\s*\r\n	#2 provi, 3 id
				povol�n�\s+(.+?)\s*\r\n						#4 povolani
				Rasa\s+(.+?)\s*\r\n							#5 rasa
				Pohlav�\s+(.+?)\s*\r\n						#6 pohlavi
				Sl�va\s+(\d+).*\r\n							#7 slava
				Po�et\ lid�\s+(\d+)\s*\r\n					#8 lidi
				Po�et\ hrad�\s+(\d+)\s*\r\n				#9 hrady
				S�la\ P\.\s+(\d+)\s*\r\n					#10 sila
				Zlato\s+(\d+)\s*\r\n							#11 zlato
				Mana\s+(\d+)\s*\r\n							#12 mana
				Rozloha\s+(\d+)\s*\r\n						#13 rozloha
				P�esv�d�en�\s+(.+?)\s*\r\n					#14 presvedceni
				Aliance\s+([^\r\n]+)							#15 ali
				/xi', $vstup, $match))
			return $match;
		return 0;
	}
	
	function smaz () {
		MySQL_Query ("DELETE FROM `koberce_users` WHERE `ID_koberce` = '{$this->id}'");
		MySQL_Query ("DELETE FROM `koberce` WHERE `ID` = '{$this->id}'");
	}
	
	function cisloSRadem ($cislo) {
		$mod = Array (1=> "k", "M", "G", "T", "P", "E");
		$rad = 0;
		
		while (($cislo / 1000 >= 1) && ($rad < 6)) {
			$cislo /= 1000;
			$rad ++;
		}
		
		return round ($cislo).$mod[$rad];
	}
	
	function novyKoberecTabulka () {
		global $user_info;
		
		echo '		
			<form action="main.php" class="novy_koberec" method="post">
			<input type="hidden" name="akce" value="koberce">
			<input type="hidden" name="typ" value="new">
			<table>
				<tr>
					<td>Text: <br>(�pehov�)</td>
					<td><textarea name="k_cil"></textarea></td>
				</tr>
				<tr>
					<td>CSka: <br>(servis)</td>
					<td><textarea name="k_cska"></textarea></td>
				</tr>
				<tr>
					<td>Poznamka: <br>(nap�. �as sest�elu atp)</td>
					<td><textarea name="k_pozn"></textarea></td>
				</tr>
				<tr>
					<td>Ve�ejn�:</td>
					<td><input type="checkbox" name="k_verejny" checked> </td>
				</tr>
				<tr>
					<td>Bounty pro 1ku:</td>
					<td><input name="k_bounty1" value="0"></td>
				</tr>
				<tr>
					<td>Bounty pro 2ku:</td>
					<td><input name="k_bounty2" value="0"></td>
				</tr>
				<tr>
					<td>Bounty pro 3ku:</td>
					<td><input name="k_bounty3" value="0"></td>
				</tr>
				<tr>
					<td>�rove�:</td>
					<td><select name="k_noob" size=2>
						<option value="1" selected>norm�ln� hr��</option>
						<option value="0">n00b</option>
					</select></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" name="submit" value="Zapsat"></td>
				</tr>
			</table>
			
			</form>';
			
		return 1;
	}
}
?>