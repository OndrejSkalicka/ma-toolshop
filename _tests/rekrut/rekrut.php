<?php
function rekrut () {
	require "analyse.php";
	require "rekrut_fce.php";
	require "rekrut_vystupy.php";
	require "kouzla.php";
	require "rekrut_jednotky.php";
	
	echo '<div id="rekrut">';
	if (!$_POST['odeslano']) include "vstup.php";
	
	if ($_POST['odeslano']) {	
		$hospodareni = zanalizuj_hosp_auto ($_POST['hospodareni_in']);
		zanalizuj_hosp_manual ($hospodareni);
		$hospodareni['kouzla_base'] = Priprav_kouzla();
		$hospodareni['kouzla'] = RozdelKouzlaNaCasti ($_POST['kouzla_in'], $hospodareni['kouzla_base']);
		$casti = RozdelVstupNaCasti ($_POST['rekrut_in']);
		if ($casti == '') return "Nen� co rekrutovat<br>";
		
		foreach ($casti as $cast) {
			if (!RekrutujJednotku ($cast, $hospodareni))
				echo "chyba p�i nakrucov�n� jednotky (chybn� jm�no, nulov� po�et...) ".$cast['jmeno']."<br>";
		}
		
		vysledky ($hospodareni);
	}
	
	
	
	echo "</div>";
	return 1;
}
?>
