<?php
class Statistiky {
	var $skript;

	function Statistiky ($skript) {
		$this->skript = $skript;
		
		
	}
	
	function stats ($co, $priorOd, $priorDo) {
		if ($priorDo < 1) 
			$priorDo = 999;
		$retVal = null;
		switch ($co) {
			case 'pocet':
				
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="30" class="head">
							Po�et bran v datab�zi
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							D
						</td>
						<td class="right">
							%
						</td>
						<td class="right">
							Z
						</td>
						<td class="right">
							%
						</td>
						<td class="right">
							N
						</td>
						<td class="right">
							%
						</td>
						<td class="right">
							Tot.
						</td>
					</tr>
				';	
				for ($i = 1; $i <= 9; $i ++ ) {
				
					$pocetTot = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i'"));
					$pocetD = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `presvedceni` = 'D'"));
					$pocetZ = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `presvedceni` = 'Z'"));
					$pocetN = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `presvedceni` = 'N'"));

					$retVal .= '<tr>
						<td>
							'.($i < 9 ? "br{$i}na" : "m�sto").'
						</td>
						<td class="right">
							'.cislo ($pocetD[0]).'
						</td>
						<td class="right">
							'.cislo ($pocetTot[0] == 0 ? 0 : $pocetD[0]/$pocetTot[0]*100).' %
						</td>
						<td class="right">
							'.cislo ($pocetZ[0]).'
						</td>
						<td class="right">
							'.cislo ($pocetTot[0] == 0 ? 0 : $pocetZ[0]/$pocetTot[0]*100).' %
						</td>
						<td class="right">
							'.cislo ($pocetN[0]).'
						</td>
						<td class="right">
							'.cislo ($pocetTot[0] == 0 ? 0 : $pocetN[0]/$pocetTot[0]*100).' %
						</td>
						<td class="right">
							'.cislo ($pocetTot[0]).'
						</td>
					</tr>
					';
				}
				$pocetTot = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo'"));
				$pocetD = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `presvedceni` = 'D'"));
				$pocetZ = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND  `presvedceni` = 'Z'"));
				$pocetN = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek` 
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND  `presvedceni` = 'N'"));
				$retVal .= '
				<tr class="bold doubleTop">
						<td>
							Celkem
						</td>
						<td class="right">
							'.cislo ($pocetD[0]).'
						</td>
						<td class="right">
							'.cislo ($pocetTot[0] == 0 ? 0 : $pocetD[0]/$pocetTot[0]*100).' %
						</td>
						<td class="right">
							'.cislo ($pocetZ[0]).'
						</td>
						<td class="right">
							'.cislo ($pocetTot[0] == 0 ? 0 : $pocetZ[0]/$pocetTot[0]*100).' %
						</td>
						<td class="right">
							'.cislo ($pocetN[0]).'
						</td>
						<td class="right">
							'.cislo ($pocetTot[0] == 0 ? 0 : $pocetN[0]/$pocetTot[0]*100).' %
						</td>
						<td class="right">
							'.cislo ($pocetTot[0]).'
						</td>
					</tr>
				';
				$retVal .= '</table>';
			break;
			case 'avgsila':
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="99" class="head">
							Pr�m�rn� s�la na br�ny
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							D
						</td>
						<td class="right">
							Z
						</td>
						<td class="right">
							N
						</td>
						<td class="right">
							Tot.
						</td>
					</tr>					
				';
				for ($i = 1; $i <= 9; $i ++ ) {
					$pocetTot = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`sila`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND  `cisloBrany` = '$i'"));
					$pocetD = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`sila`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND  `cisloBrany` = '$i' AND `presvedceni` = 'D'"));
					$pocetZ = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`sila`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND  `cisloBrany` = '$i' AND `presvedceni` = 'Z'"));
					$pocetN = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`sila`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND  `cisloBrany` = '$i' AND `presvedceni` = 'N'"));
					$retVal .= '<tr>
						<td>
							'.($i < 9 ? "br{$i}na" : "m�sto").'
						</td>
						<td class="right">
							'.cislo($pocetD[0]).'
						</td>
						<td class="right">
							'.cislo($pocetZ[0]).'
						</td>
						<td class="right">
							'.cislo($pocetN[0]).'
						</td>
						<td class="right">
							'.cislo($pocetTot[0]).'
						</td>
					</tr>';
				}
				$retVal .= '</table>';
			break;
			case 'avgot':
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="99" class="head">
							Pr�m�rn� OT na br�ny
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							D
						</td>
						<td class="right">
							Z
						</td>
						<td class="right">
							N
						</td>
						<td class="right">
							Tot.
						</td>
					</tr>					
				';
				for ($i = 1; $i <= 9; $i ++ ) {
					$pocetTot = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i'"));
					$pocetD = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `presvedceni` = 'D'"));
					$pocetZ = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `presvedceni` = 'Z'"));
					$pocetN = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`) FROM `archiv_branek`  
																			 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																	 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `presvedceni` = 'N'"));
					$retVal .= '<tr>
						<td>
							'.($i < 9 ? "br{$i}na" : "m�sto").'
						</td>
						<td class="right">
							'.cislo($pocetD[0]).'
						</td>
						<td class="right">
							'.cislo($pocetZ[0]).'
						</td>
						<td class="right">
							'.cislo($pocetN[0]).'
						</td>
						<td class="right">
							'.cislo($pocetTot[0]).'
						</td>
					</tr>';
				}
				$retVal .= '</table>';
			break;
			case 'povolaniOT':
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="99" class="head">
							Pr�m�rn� OT na br�ny podle povol�n�
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							br1na
						</td>
						<td class="right">
							br2na
						</td>
						<td class="right">
							br3na
						</td>
						<td class="right">
							br4na
						</td>
						<td class="right">
							br5na
						</td>
						<td class="right">
							br6na
						</td>
						<td class="right">
							br7na
						</td>
						<td class="right">
							br8na
						</td>
						<td class="right">
							m�sto
						</td>
					</tr>					
				';
				$povolani = array ('Alchymista', 'Barbar', 'Druid', 'Hrani���', 'Iluzionista', 'Klerik', 'M�g', 'Nekromant', 'Theurg', 'V�le�n�k');
				/* vsechny presvedceni x povolani */
				foreach ($povolani as $povol) {
					$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", 'celkem' => '1');
					foreach ($presvedceni as $pKey => $pValue) {
						$retVal .= '
						<tr'.($pValue == '1' ? ' class="doubleBot"' : '').'>
						<td>
							'.$povol.' '.$pKey.'
						</td>';
						for ($i = 1; $i <= 9; $i ++ ) {
							$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`) FROM `archiv_branek`  
																					 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																			 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `povolani` = '$povol' AND {$pValue}"));
							$retVal .= '
							<td class="right">
								'.cislo($pocet[0]).'
							</td>
							';
						}
						$retVal .= '</tr>						
						';
					}
				}
				/* celkem pro vsechny presvedceni */
				$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", '' => '1');
				foreach ($presvedceni as $pKey => $pValue) {
					$retVal .= '
					<tr class="bold'.($pValue == '1' ? ' doubleTop' : '').'">
					<td>
						Celkem '.$pKey.'
					</td>';
					for ($i = 1; $i <= 9; $i ++ ) {
						$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND {$pValue}"));
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>
						';
					}
					$retVal .= '</tr>						
					';
				}
				$retVal .= '</table>';
			break;
			case 'povolaniOT2':
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="99" class="head">
							Pr�m�rn� OT na br�ny podle povol�n�
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							br1na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br2na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br3na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br4na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br5na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br6na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br7na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br8na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							m�sto
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							&sum; 1-9
						</td>
					</tr>			
				';
				$povolani = array ('Alchymista', 'Barbar', 'Druid', 'Hrani���', 'Iluzionista', 'Klerik', 'M�g', 'Nekromant', 'Theurg', 'V�le�n�k');
				/* vsechny presvedceni x povolani */
				foreach ($povolani as $povol) {
					$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", 'celkem' => '1');
					foreach ($presvedceni as $pKey => $pValue) {
						$retVal .= '
						<tr'.($pValue == '1' ? ' class="doubleBot"' : '').'>
						<td>
							'.$povol.' '.$pKey.'
						</td>';
						for ($i = 1; $i <= 9; $i ++ ) {
							$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`), COUNT(*) FROM `archiv_branek`  
																					 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																			 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `povolani` = '$povol' AND {$pValue}"));
							$retVal .= '
							<td class="right">
								'.cislo($pocet[0]).'
							</td>
							<td class="right">
								'.cislo($pocet[1]).'
							</td>
							';
						}
						$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `povolani` = '$povol' AND {$pValue}"));
						
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>
						</tr>						
						';
					}
				}
				/* celkem pro vsechny presvedceni */
				$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", '' => '1');
				foreach ($presvedceni as $pKey => $pValue) {
					$retVal .= '
					<tr class="bold'.($pValue == '1' ? ' doubleTop' : '').'">
					<td>
						V�echny '.$pKey.'
					</td>';
					for ($i = 1; $i <= 9; $i ++ ) {
						$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`OT`), COUNT(*) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND {$pValue}"));
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>						
						<td class="right">
							'.cislo($pocet[1]).'
						</td>
						';
					}
					$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND {$pValue}"));
						
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>
						</tr>						
						';
				}
				$retVal .= '
					<tr class="italic">
						<td>
							&nbsp;
						</td>
						<td class="right">
							br1na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br2na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br3na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br4na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br5na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br6na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br7na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br8na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							m�sto
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							&sum; 1-9
						</td>
					</tr>
					</table>';
			break;			
			case 'povolaniSila2':
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="99" class="head">
							Pr�m�rn� s�la na br�ny podle povol�n�
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							br1na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br2na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br3na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br4na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br5na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br6na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br7na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br8na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							m�sto
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							&sum; 1-9
						</td>
					</tr>			
				';
				$povolani = array ('Alchymista', 'Barbar', 'Druid', 'Hrani���', 'Iluzionista', 'Klerik', 'M�g', 'Nekromant', 'Theurg', 'V�le�n�k');
				/* vsechny presvedceni x povolani */
				foreach ($povolani as $povol) {
					$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", 'celkem' => '1');
					foreach ($presvedceni as $pKey => $pValue) {
						$retVal .= '
						<tr'.($pValue == '1' ? ' class="doubleBot"' : '').'>
						<td>
							'.$povol.' '.$pKey.'
						</td>';
						for ($i = 1; $i <= 9; $i ++ ) {
							$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`Sila`), COUNT(*) FROM `archiv_branek`  
																					 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																			 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `povolani` = '$povol' AND {$pValue}"));
							$retVal .= '
							<td class="right">
								'.cislo($pocet[0]).'
							</td>
							<td class="right">
								'.cislo($pocet[1]).'
							</td>
							';
						}
						$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `povolani` = '$povol' AND {$pValue}"));
						
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>
						</tr>						
						';
					}
				}
				/* celkem pro vsechny presvedceni */
				$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", '' => '1');
				foreach ($presvedceni as $pKey => $pValue) {
					$retVal .= '
					<tr class="bold'.($pValue == '1' ? ' doubleTop' : '').'">
					<td>
						V�echny '.$pKey.'
					</td>';
					for ($i = 1; $i <= 9; $i ++ ) {
						$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`Sila`), COUNT(*) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND {$pValue}"));
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>						
						<td class="right">
							'.cislo($pocet[1]).'
						</td>
						';
					}
					$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT COUNT(*) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND {$pValue}"));
						
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>
						</tr>						
						';
				}
				$retVal .= '
					<tr class="italic">
						<td>
							&nbsp;
						</td>
						<td class="right">
							br1na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br2na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br3na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br4na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br5na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br6na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br7na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							br8na
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							m�sto
						</td>
						<td class="right">
							&sum;
						</td>
						<td class="right">
							&sum; 1-9
						</td>
					</tr>
					</table>';
			break;
			case 'povolaniSila':
				$retVal .= '
				<table class="bordered">
					<tr>
						<td colspan="99" class="head">
							Pr�m�rn� s�la na br�ny podle povol�n�
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="right">
							br1na
						</td>
						<td class="right">
							br2na
						</td>
						<td class="right">
							br3na
						</td>
						<td class="right">
							br4na
						</td>
						<td class="right">
							br5na
						</td>
						<td class="right">
							br6na
						</td>
						<td class="right">
							br7na
						</td>
						<td class="right">
							br8na
						</td>
						<td class="right">
							m�sto
						</td>
					</tr>					
				';
				$povolani = array ('Alchymista', 'Barbar', 'Druid', 'Hrani���', 'Iluzionista', 'Klerik', 'M�g', 'Nekromant', 'Theurg', 'V�le�n�k');
				foreach ($povolani as $povol) {
					$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", 'celkem' => '1');
					foreach ($presvedceni as $pKey => $pValue) {
						$retVal .= '
						<tr'.($pValue == '1' ? ' class="doubleBot"' : '').'>
						<td>
							'.$povol.' '.$pKey.'
						</td>';
						for ($i = 1; $i <= 9; $i ++ ) {
							$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`sila`) FROM `archiv_branek`  
																					 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																			 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND `povolani` = '$povol' AND {$pValue}"));
							$retVal .= '
							<td class="right">
								'.cislo($pocet[0]).'
							</td>
							';
						}
						$retVal .= '</tr>						
						';
					}
				}
				/* celkem pro vsechny presvedceni */
				$presvedceni = array ('D' => "`presvedceni` = 'D'", 'Z' => "`presvedceni` = 'Z'", 'N' => "`presvedceni` = 'N'", '' => '1');
				foreach ($presvedceni as $pKey => $pValue) {
					$retVal .= '
					<tr class="bold'.($pValue == '1' ? ' doubleTop' : '').'">
					<td>
						Celkem '.$pKey.'
					</td>';
					for ($i = 1; $i <= 9; $i ++ ) {
						$pocet = MySQL_Fetch_Array (MySQL_Query ("SELECT AVG(`sila`) FROM `archiv_branek`  
																				 INNER JOIN `veky` ON `veky`.`ID` = `archiv_branek`.`ID_veky`
																		 	 	 WHERE `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo' AND `cisloBrany` = '$i' AND {$pValue}"));
						$retVal .= '
						<td class="right">
							'.cislo($pocet[0]).'
						</td>
						';
					}
					$retVal .= '</tr>						
					';
				}
				$retVal .= '</table>';
			break;
		}
		return $retVal;
	}
}
?>