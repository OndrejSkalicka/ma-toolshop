<?php /* Smarty version 2.6.14, created on 2007-02-15 11:22:11
         compiled from spravaMistnosti.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'spravaMistnosti.tpl', 101, false),array('modifier', 'date', 'spravaMistnosti.tpl', 125, false),)), $this); ?>
<span style="text-align: center"><h2> Spr�va m�stnosti `<?php echo $this->_tpl_vars['mistnost']->getJmeno(); ?>
`</h2></span>
<fieldset>
  <legend>Zm�na hesla</legend>
  <table>
    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>
" method="post">
      <input type="hidden" name="akce" value="<?php echo $_REQUEST['akce']; ?>
" />
      <input type="hidden" name="chid" value="<?php echo $_REQUEST['chid']; ?>
" />
      <input type="hidden" name="spravovat" value="1" />
      <tr>
        <td>Star�</td>
        <td><input type="password" name="old"/></td>
      </tr>
      <tr>
        <td>Nov�</td>
        <td><input type="password" name="new1"/></td>
      </tr>
      <tr>
        <td>Nov� znova</td>
        <td><input type="password" name="new2"/></td>
      </tr>
      <tr>
        <td>Sma� m�stnost!</td>
        <td><input type="checkbox" name="zrus_mistnost" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="zmenaHesla" value="Zm��"/></td>
      </tr>
    </form>
  </table>
</fieldset>
<!-- spravci -->
<fieldset>
  <legend>Spr�vci</legend>
  <table>
    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>
" method="post">
      <input type="hidden" name="akce" value="<?php echo $_REQUEST['akce']; ?>
" />
      <input type="hidden" name="chid" value="<?php echo $_REQUEST['chid']; ?>
" />
      <input type="hidden" name="spravovat" value="1" />
      <tr>
        <td colspan="3"><strong>P�idej spr�vce</strong></td>
      </tr>
      <tr>
        <td>ID:</td>
        <td><input name="newSpravceId"/></td>
        <td><input type="submit" name="newSpravce" value="Nov� spr�vce"/></td>
      </tr>
      <tr>
        <td colspan="3"><strong>Spr�vci m�stnosti</strong></td>
      </tr>
      <tr>
        <td>ID</td>
        <td>Regent</td>
        <td>Odstranit</td>
      </tr>
      <?php $_from = $this->_tpl_vars['mistnost']->getSpravci(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['spravce']):
?>
      <tr>
        <td class="right"><?php echo $this->_tpl_vars['spravce']['login']; ?>
</td>
        <td><?php echo $this->_tpl_vars['spravce']['regent']; ?>
, <?php echo $this->_tpl_vars['spravce']['provi']; ?>
</td>
        <td>
        <input type="checkbox" name="zrusSpravceLogin[<?php echo $this->_tpl_vars['spravce']['id']; ?>
]"/></td>
      </tr>
      <?php endforeach; else: ?>
      <tr>
        <td colspan="3">��dn�</td>
      </tr>
      <?php endif; unset($_from); ?>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td><input type="submit" name="zrusSpravce" value="Zru� spr�vce!" onClick='return window.confirm("Opravdu zru�it spr�vce?")'/></td>
      </tr>
    </form>
  </table>
</fieldset> 
<fieldset>
  <legend>Bany</legend>
  <table>
    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>
" method="post">
      <input type="hidden" name="akce" value="<?php echo $_REQUEST['akce']; ?>
" />
      <input type="hidden" name="chid" value="<?php echo $_REQUEST['chid']; ?>
" />
      <input type="hidden" name="spravovat" value="1" />
      <tr>
        <td colspan="7">
          <strong>Nov� ban!</strong>
        </td>
      </tr>
      <tr>
        <td>
          ID:
        </td>
        <td>
          <input name="newBanLogin" />
        </td>
        <td>
          d�vod:
        </td>
        <td>
          <input name="newBanReason" />
        </td>
        <td>Na jak dlouho:</td>
        <td colspan="2"><?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['bananyMoznosti'],'name' => 'newBanExpire'), $this);?>
</td>
        <td><input type="submit" name="pridejBan" value="P�idej bany!" onClick='return window.confirm("Opravdu p�idat ban?")'/></td>
      </tr>
    </table>
    <table>
      <tr>
        <td colspan="6">
          <strong>Zabanovan� u�ivatel�</strong>
        </td>
      </tr>
      <tr>
        <td>ID</td>
        <td>Zabanovan� regent</td>
        <td>Autor banu</td>
        <td>D�vod</td>
        <td>Kon��</td>
        <td>Zru�it</td>
      </tr>
      <?php $_from = $this->_tpl_vars['mistnost']->getBany(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ban']):
?>
      <tr>
        <td class="right"><?php echo $this->_tpl_vars['ban']['p_login']; ?>
</td>
        <td><?php echo $this->_tpl_vars['ban']['p_regent']; ?>
, <?php echo $this->_tpl_vars['ban']['p_provi']; ?>
</td>
        <td><?php echo $this->_tpl_vars['ban']['s_regent']; ?>
 (<?php echo $this->_tpl_vars['ban']['s_login']; ?>
)</td>
        <td><?php echo $this->_tpl_vars['ban']['reason']; ?>
</td>
        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['ban']['expire'])) ? $this->_run_mod_handler('date', true, $_tmp) : smarty_modifier_date($_tmp)); ?>
</td>
        <td><input type="checkbox" name="zrusBanId[<?php echo $this->_tpl_vars['ban']['b_id']; ?>
]"/></td>
      </tr>
      <?php endforeach; else: ?>
      <tr>
        <td colspan="5" align="center">��dn�</td>
      </tr>
      <?php endif; unset($_from); ?>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td><input type="submit" name="zrusBany" value="Zru� bany!" onClick='return window.confirm("Opravdu zru�it bany?")'/></td>
      </tr>
    </form>
  </table>
</fieldset>