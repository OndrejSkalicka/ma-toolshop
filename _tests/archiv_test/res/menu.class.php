<?php
class Menu {
	var $aktivni, $skript, $polozky, $user,
		 $lastVolba /* posledni vykreslena class metodou vykresliVolbu */,
		 $lowStatusMsg = "Pouze pro registrovan� u�ivetele! (pop�. nem�te dostate�n� opr�vn�n�)";
	
	function Menu ($skript, &$user, $aktivni = null) {
		$this->skript = $skript;
		$this->aktivni = $aktivni;
		$this->nastavPolozky();
		$this->user = &$user;
	}
	
	function vykonejVolbu ($aktivni = null) {
		if (is_null($aktivni)) $aktivni = $this->aktivni;
		switch ($aktivni) {
			/*case $this->polozky["logout"]->akce:
				$this->user->logout(true);
			break;*/
			case $this->polozky['pridavani']->akce:
				if ($_REQUEST['zpetStep1']) $_REQUEST['step'] = 1;
				switch ($_REQUEST['step']) {
					case 3: $temp = 'Ulo�en� do datab�ze'; break;
					case 2: $temp = 'Potvrzen�'; break;
					default: $temp = 'Zad�v�n�'; break;
				}
				$this->vykresliVolbuStart('Krok '.(min(max(1,$_REQUEST['step']), 3)).'/3 - '.$temp, "archiv_pridavani");
				if ($this->polozky['pridavani']->minStatus <= $this->user->status()) {
				
					$branka = new Branka ($this->user, $this->skript, $aktivni);
					$branka->vykresliPridavaciTabulku($_REQUEST['step']);
					/*echo '</div> <!-- //archiv_pridavani -->';*/
				} else echo $this->lowStatusMsg;
				$this->vykresliVolbuStop();
			break;
			case $this->polozky['branky']->akce: 				
								
				if ($_REQUEST['simul'] > 0) $title = 'Simulace';
				elseif ($_REQUEST['show'] > 0) $title = 'Detail br�ny';
				elseif ($_POST['hledej']) $title = 'V�sledky hled�n�';
				else $title = 'Vyhled�v�n� bran';
				
				$this->vykresliVolbuStart($title, 'archiv_hledani');
				
				if ($this->polozky['branky']->minStatus <= $this->user->status()) {
					if ($_REQUEST['simul'] > 0) {
						$br_id = $_REQUEST['simul'];
						$branka = new Branka($this->user, $this->skript, $aktivni, $br_id);
						$cisloBranky = $branka->cisloBrany;
						if ($branka->boj->presvedceni == 'Z') $cisloBranky += 10;
						if ($branka->boj->presvedceni == 'N') $cisloBranky += 20;
						
						$simul = new Simul ();
						$error = $simul->nastavParametry($cisloBranky, $br_id, $_REQUEST['vek']);
						
						if (!$error) {						
							/* $branka->boj->vykresliTabulkuJednotek($i) */
							echo '
							<div id="archiv_simul">							
								<div class="ramec">
									<div class="kolo">Simul (1. TU):<br /><br />'.
										$simul->odehrajKolo (1).'
									</div> <!-- //kolo -->
									<div class="kolo">Real (1. TU):<br /><br />'.
										$branka->boj->vykresliTabulkuJednotek(1).'
									</div> <!-- //kolo -->
									<div class="kolo">Simul (2. TU):<br /><br />'.
										$simul->odehrajKolo (2).'
									</div> <!-- //kolo -->
									<div class="kolo">Real (2. TU):<br /><br />'.
										$branka->boj->vykresliTabulkuJednotek(2).'
									</div> <!-- //kolo -->
									<div class="clear">
										&nbsp;
									</div> <!-- // -->
								</div> <!-- //ramec -->
								<br />
								<div class="ramec">
									<div class="kolo">Simul (3. TU):<br /><br />'.
										$simul->odehrajKolo (3).'
									</div> <!-- //kolo -->
									<div class="kolo">Real (3. TU):<br /><br />'.
										$branka->boj->vykresliTabulkuJednotek(3).'
									</div> <!-- //kolo -->
									<div class="kolo">Simul (4. TU):<br /><br />'.
										$simul->odehrajKolo (4).'
									</div> <!-- //kolo -->
									<div class="kolo">Real (4. TU):<br /><br />'.
										$branka->boj->vykresliTabulkuJednotek(4).'
									</div> <!-- //kolo -->
									<div class="clear">
										&nbsp;
									</div> <!-- //clear -->
								</div> <!-- //ramec -->
								<br />
								<div class="ramec">
									<table width="100%" >
									<tr>
										<td align="center">
											Simul: ';
							$sZtraty = $simul->vysledek();
							$branka->vykresliVysledek ($sZtraty['utok'] * 100, $sZtraty['obrana'] * 100);
							echo '
										</td>
										<td align="center">
											Real: ';
							$branka->vykresliVysledek ($branka->boj->ztraty['utok'], $branka->boj->ztraty['obrana']);
							echo '
										</td>
									</tr>
									</table>
									
								</div> <!-- //ramec -->
							</div> <!-- //archiv_simul -->
							';
						} 
						else 
							echo "$error";
					}
					elseif ($_REQUEST['show'] > 0) {
						$branka = new Branka($this->user, $this->skript, $aktivni, $_REQUEST['show']);
						if (!$branka->error)
							$branka->vykresliBranku(1, 1);
					} else {
						$vyhledavac = new VyhledavaniBranek ($this->skript, $aktivni, $this->user);
						if ($_POST['hledej']) 
							$vyhledavac->vypisBrany($vyhledavac->vyhledejBrany());
						else 
							$vyhledavac->vykresliHledaciTabulku();
					}
				} else echo $this->lowStatusMsg;
				$this->vykresliVolbuStop();
			break;
			case $this->polozky['vlastniBranky']->akce:
				/* HEAD */
				
				if ($_REQUEST['show'] > 0) $title = 'Vlastn� br�ny - detail';
				else $title = 'Vlastn� br�ny';
				$this->vykresliVolbuStart($title, 'archiv_vlastni');
					
				if ($this->polozky['vlastniBranky']->minStatus <= $this->user->status()) {
					
					/* BODY */
					if ($_REQUEST['show'] > 0) {
						$branka = new Branka($this->user, $this->skript, $aktivni, $_REQUEST['show']);
						if ($branka->userID == $this->user->userID) {
							$branka->vykresliUpdatovaciTabulku ();
							$branka->vykresliBranku(1);
						} else {
							echo '<div class="error">
								Nem�te pr�va na upravov�n� t�to br�ny
							</div> <!-- //error -->';
						}
					} else {
						if ($_POST['cil']) { //upravovani/mazani
							$branka = new Branka ($this->user, $this->skript, $aktivni, $_POST['cil']);
							if ($branka->user->userID == $this->user->userID) {
								if ($_POST['smazat'])
									if ($branka->smaz()) 
										echo '<div class="ok">
											Br�na byla smaz�na.
										</div> <!-- //ok -->';
									else echo '<div class="error">
										Chyba p�i maz�n� br�ny!
									</div> <!-- //error -->';
								if ($_POST['upravit']) {
									if ($branka->uprav())
										echo '<div class="ok">
											Br�na byla upravena.
										</div> <!-- //ok -->';
									else echo '<div class="error">
										Chyba p�i upravov�n� br�ny!
									</div> <!-- //error -->';
								}
							} else {
								echo '<div class="error">
									Na upravov�n� t�to branky nem�te pr�va!	
								</div> <!-- // -->';
							}
						}
						$vyhledavac = new VyhledavaniBranek ($this->skript, $aktivni, $this->user);
						$vyhledavac->vypisBrany($vyhledavac->vyhledejVlastniBrany($this->user->userID), true);
					}					
				} else echo $this->lowStatusMsg;
				
				/* TAIL */
				$this->vykresliVolbuStop();
			break;
			case $this->polozky['nastaveni']->akce:
				/* HEAD */
				$title = 'Nastaven�';
				$this->vykresliVolbuStart($title, 'archiv_nastaveni');
				
//				$this->user->setSablona ();
				
					
				if ($this->polozky['nastaveni']->minStatus <= $this->user->status()) {
					
					/* BODY */
					$nastaveni = new Nastaveni ($this->skript, $this->user);
					$nastaveni->vykresliTabulkuNastaveni();
					
					
				} else echo $this->lowStatusMsg;
				
				/* TAIL */
				$this->vykresliVolbuStop();
			break;
			case $this->polozky['stats']->akce:
				/* HEAD */
				$title = 'Statistiky';
				$this->vykresliVolbuStart($title, 'archiv_statistiky');
				
//				$this->user->setSablona ();
				
					
				if ($this->polozky['stats']->minStatus <= $this->user->status()) {
					
					/* BODY */
					$stats = new Statistiky ($this->skript);
					
					if (!isset($_POST['vek_priorod'])) {
						$temp = MySQL_Fetch_Array (MySQL_Query ("SELECT MAX(`priorita`) FROM `veky`"));
						$_POST['vek_priorod'] = $temp[0];
					}
					
					if (!isset($_POST['vek_priordo'])) 
						$_POST['vek_priordo'] = 1;

					echo '
					<form action="'.$this->skript.'" method="post">
						<input type="hidden" name="akce" value="'.$_REQUEST['akce'].'">
						Statistiky k v�k�m od 
						<select name="vek_priorod">';
					$veky = MySQL_Query ("SELECT * FROM `veky` ORDER BY `priorita`");	
					while ($vek = MySQL_Fetch_Array ($veky)) {
						echo '<option value="'.$vek['priorita'].'"'.($vek['priorita'] == $_POST['vek_priorod'] ? ' selected' : '').'>'.$vek['jmeno'].' ('.$vek['title'].')</option>';
					}
					echo'	</select>
					do
						<select name="vek_priordo">';
					$veky = MySQL_Query ("SELECT * FROM `veky` ORDER BY `priorita`");	
					while ($vek = MySQL_Fetch_Array ($veky)) {
						echo '<option value="'.$vek['priorita'].'"'.($vek['priorita'] == $_POST['vek_priordo'] ? ' selected' : '').'>'.$vek['jmeno'].' ('.$vek['title'].')</option>';
					}
					echo'	</select> (v�etn�)
					<input type="submit" value="Zobraz">
					</form>
					';
					/* `veky`.`priorita` <= '$priorOd' AND `veky`.`priorita` >= '$priorDo */
					echo '<table class="mainT">
					<!-- <tr>
						<td>
							'.$stats->stats('pocet', $_POST['vek_priorod'], $_POST['vek_priordo']).'
						</td>
						<td>
							'.$stats->stats('avgsila', $_POST['vek_priorod'], $_POST['vek_priordo']).'
						</td>
						<td>
							'.$stats->stats('avgot', $_POST['vek_priorod'], $_POST['vek_priordo']).'
						</td>
					</tr>-->
					<tr>
						<td colspan="3">
							'.$stats->stats('povolaniOT2', $_POST['vek_priorod'], $_POST['vek_priordo']).'
						</td>
					</tr>
					<tr>
						<td colspan="3">
							'.$stats->stats('povolaniSila2', $_POST['vek_priorod'], $_POST['vek_priordo']).'
						</td>
					</tr>
					</table>
					';
					
					
					
				} else echo $this->lowStatusMsg;
				
				/* TAIL */
				$this->vykresliVolbuStop();
			break;
			case $this->polozky['help']->akce:
				/* HEAD */
				$title = 'N�pov�da';
				$this->vykresliVolbuStart($title, 'archiv_help');
				
//				$this->user->setSablona ();
				
					
				if ($this->polozky['help']->minStatus <= $this->user->status()) {
					
					/* BODY */
					/*$nastaveni = new Nastaveni ($this->skript, $this->user);
					$nastaveni->vykresliTabulkuNastaveni();*/
					include 'help.html';
					
					
				} else echo $this->lowStatusMsg;
				
				/* TAIL */
				$this->vykresliVolbuStop();
			break;
			case 'simul':
				/* HEAD */
				$title = 'Simul�tor Bran';
				$this->vykresliVolbuStart($title, 'archiv_simul');
				
					
				if ($this->polozky['simul']->minStatus <= $this->user->status()) {					
					/* BODY */
					$br_id = 30;
					$branka = new Branka($this->user, $this->skript, $aktivni, $br_id);
					$cisloBranky = $branka->cisloBrany;
					if ($branka->boj->presvedceni == 'Z') $cisloBranky += 10;
					if ($branka->boj->presvedceni == 'N') $cisloBranky += 20;
					
					$simul = new Simul ();
					$simul->nastavParametry($cisloBranky, $br_id, $branka->vekID);
					
					
					
					/* $branka->boj->vykresliTabulkuJednotek($i) */
					echo '
					<div class="ramec">
						<div class="kolo">Simul:<br />'.
							$simul->odehrajKolo (1).'
						</div> <!-- //kolo -->
						<div class="kolo">Real:<br />'.
							$branka->boj->vykresliTabulkuJednotek(1).'
						</div> <!-- //kolo -->
						<div class="kolo">Simul:<br />'.
							$simul->odehrajKolo (2).'
						</div> <!-- //kolo -->
						<div class="kolo">Real:<br />'.
							$branka->boj->vykresliTabulkuJednotek(2).'
						</div> <!-- //kolo -->
						<div class="clear">
							&nbsp;
						</div> <!-- // -->
					</div> <!-- //ramec -->
					<br />
					<div class="ramec">
						<div class="kolo">Simul:<br />'.
							$simul->odehrajKolo (3).'
						</div> <!-- //kolo -->
						<div class="kolo">Real:<br />'.
							$branka->boj->vykresliTabulkuJednotek(3).'
						</div> <!-- //kolo -->
						<div class="kolo">Simul:<br />'.
							$simul->odehrajKolo (4).'
						</div> <!-- //kolo -->
						<div class="kolo">Real:<br />'.
							$branka->boj->vykresliTabulkuJednotek(4).'
						</div> <!-- //kolo -->
						<div class="clear">
							&nbsp;
						</div> <!-- // -->
					</div> <!-- //ramec -->
					';
					
				} else echo $this->lowStatusMsg;
				
				/* TAIL */
				$this->vykresliVolbuStop();			
			break;
			case 'registrace':
				/* HEAD */
				$title = 'Registrace';
				$this->vykresliVolbuStart($title, 'archiv_registrace');
				
				$registrace = new Registrace ($this->skript);
				if ($registrace->registruj()) {
					echo "Va�e registrace prob�hla �sp�n�. Nyn� se m��ete p�ihl�sit.";
				} else {
					$registrace->vykresliRegistracniTabulku();				
				}
				
				/* TAIL */
				$this->vykresliVolbuStop();				
			break;
			default:
				$this->vykresliVolbuStart('N/A yet', "null");				
				echo "comming soon";
				$this->vykresliVolbuStop();
			break;
		}		
	}
	
	/* vykresli DIVy pro volbu -> tzn. celou spodni cast stranky */
	function vykresliVolbuStart ($title, $class) {
		$this->lastVolba = $class;
		echo '<div id="'.$this->lastVolba.'">
			<div class="archiv_nadpis">
				'.$title.'
			</div> <!-- //archiv_nadpis -->';
	}
	
	function vykresliVolbuStop () {
		echo '</div> <!-- //'.$this->lastVolba.' -->';
	}
	
	function nastavPolozky () {
		$this->polozky = array ("branky" => new PolozkaMenu("Branky", '', 0),
										"help" => new PolozkaMenu("Help", 'help', 0 ),
				 						"pridavani" => new PolozkaMenu("P�id�v�n�", 'pridavani', 100),
										"vlastniBranky" => new PolozkaMenu("Vlastn� branky", 'vlastni', 100),
										"stats" => new PolozkaMenu("Statistiky", 'stats', 0),
										"nastaveni" => new PolozkaMenu("Nastaven�", 'nastaveni', 0)/*,
										"simul" => new PolozkaMenu("Simul�tor branek", 'simul', 100)*/);
	}
	
	function vykresliPolozku ($polozka, $selected = false) {
		echo '
		<div class="archiv_polozka">
			<a href="'.$this->skript.($polozka->akce ? '?akce='.$polozka->akce : '').'"><span'.($selected ? ' class="selected"' : '').'>'.$polozka->title.'</span></a>
		</div> <!-- //archiv_polozka -->';
	}
	
	function vykresliMenu () {
		echo '<div id="archiv_menu">';		
		foreach ($this->polozky as $polozka) {
			$this->vykresliPolozku ($polozka, $this->aktivni == $polozka->akce);
		}
		echo '</div> <!-- //archiv_menu -->
		<div class="clear">&nbsp;</div>';
	}
}
?>