<?php 
/* overeni uzivatele */
require_once ("fce.php");
if (!CheckLogin () || !MaPrava ("aukce")) {
	LogOut();
}
/* ------ */

if (!IsSet($_SESSION['P'])) {
	$_SESSION['P'] = "checked";
	$_SESSION['L'] = "checked";
	$_SESSION['B'] = "checked";
	$_SESSION['S'] = "checked";
} elseif ($_POST['send'] == 1) {
	if ($_POST['P'] == "on") {
		$_SESSION['P'] = "checked";
	} else {
		$_SESSION['P'] = "";
	}
	if ($_POST['L'] == "on") {
		$_SESSION['L'] = "checked";
	} else {
		$_SESSION['L'] = "";
	}
	if ($_POST['B'] == "on") {	
		$_SESSION['B'] = "checked";
	} else {
		$_SESSION['B'] = "";
	} 
	if ($_POST['S'] == "on") {
		$_SESSION['S'] = "checked";
	} else {
		$_SESSION['S'] = "";
	}
	if (Is_Numeric ($_POST['cost_2']) || ($_POST['cost_2'] == "")) {
		$_SESSION['cost_2'] = $_POST['cost_2'];
	}
	if (Is_Numeric ($_POST['min_pwr']) || ($_POST['min_pwr'] == "")) {
		$_SESSION['min_pwr'] = $_POST['min_pwr'];
	}
}
$P = $_SESSION['P'];
$L = $_SESSION['L'];
$B = $_SESSION['B'];
$S = $_SESSION['S'];
$cost_2 = $_SESSION['cost_2'];
$min_pwr = $_SESSION['min_pwr'];
?>
<form action="main.php" method="post">
<input type="hidden" name="akce" value="aukce">
<div id="aukce_left">
<?php 
echo "
<table>
<tr>
	<td>
		<input type=\"checkbox\" name=\"P\" ID=\"P\" $P><label for=\"P\">Pozemn�</label>
	</td>
	<td>
		<input type=\"checkbox\" name=\"B\" ID=\"B\" $B><label for=\"B\">Bojov�</label>
	</td>
</tr>
<tr>
	<td>
		<input type=\"checkbox\" name=\"L\" ID=\"L\" $L><label for=\"L\">Leteck�</label>
	</td>
	<td>
		<input type=\"checkbox\" name=\"S\" ID=\"S\" $S><label for=\"S\">St�eleck�</label>
	</td>
</tr>
<tr>
	<td>
		Max. cost: 
	</td>
	<td>
		<input name=\"cost_2\" value=\"$cost_2\">
	</td>
</tr>
<tr>
	<td>
		Min. pwr:
	</td><td>
		<input name=\"min_pwr\" value=\"$min_pwr\">
	</td>
</tr>
</table>";
?>
<br>
<input type="submit" value="Odeslat">
<input type="hidden" name="send" value="1">
</div>
<div id="aukce_right">
	(CTRL+A, CTRL+C, CTRL+V z aukce, pokud nem�te, tak <a href="aukce/obchod.html" target="_blank" style="color: red">tady</a> na test)
	<textarea name="aukce" rows="8" cols="50"><?php echo $_POST['aukce'];?></textarea><br>
</div>
</form>
<div class="clear"></div>
<div id="aukce_bottom">
<table>
<tr>
	<td>
		Po�et
	</td>
	<td>
		Jm�no
	</td>
	<td>
		Typ
	</td>
	<td align="right">
		Zku�enost
	</td>
	<td align="right">
		<b>S�la</b>
	</td>
	<td align="right">
		Min. nab�dka
	</td>
	<td align="right">
		<b>Cena za 1k s�ly</b>
	</td>
</tr>
<?php
require "./aukce/fce.php";
if ($_POST['aukce'] != "") {
	IncDB("aukce_count");

	/* 					1. jmeno           2.pocet 3-4. XP          5-6. sila       7.druh           8. typ         9.cas						   10.nabidka*/
	preg_match_all ('/(.*?)\s+[ZSMCNB]\s+(\d+)\s+(\d+(\.\d+)?)%\s+(\d+(\.\d+)?)\s+(Poz\.|Let\.)\s+(Boj\.|Str\.)\s+(��dn� nab�dka|\d+:\d+)\s+(\d+)/', $_POST['aukce'], $matches);

	$sila_celkem = 0;
	$zl_celkem = 0;
	for ($i = 0; $matches[1][$i]; $i ++) {
		$temp = "";
		for ($j = 0; $matches[$j][$i]; $j ++) {
			$temp[] = $matches[$j][$i];
		}
		VytiskniRadek ($temp);
		$sila_celkem += $temp[2] * $temp[3] * $temp[5] / 100;
		$zl_celkem += $temp[10];
	}
	echo "
	<tr>
		<td colspan=\"7\"><hr></td>
	</tr>
	<tr>
		<td>-</td>
		<td>-</td>
		<td>-</td>
		<td align=\"right\">-</td>
		<td align=\"right\"><b>".cislo ($sila_celkem)."</b></td>
		<td align=\"right\">".cislo($zl_celkem)."</td>
		<td align=\"right\"><b>".cislo($zl_celkem/$sila_celkem*1000)."</b></td>
	</tr>";
	
}
?>
</table>