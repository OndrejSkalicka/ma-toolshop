<?php
/* overeni uzivatele */
require_once ("fce.php");
if (!CheckLogin ()) {
	LogOut();
}
/* ------ */
if ($_POST['typ'] == 'zmen_usera') {
	if (!get_magic_quotes_gpc ()) {
		$_POST['ch_popis'] = AddSlashes ($_POST['ch_popis']);
	}
	$err = 0;
	if ($_POST['ch_pwd'] != $_POST['ch_pwd2']) {
		echo "Hesla se neshoduj�.<br>";
		$err = 1;
	}
	if ($err == 0) {
		$update = "UPDATE `users` SET 
												`popis` = '".$_POST['ch_popis']."',
												`icq` = '".$_POST['ch_icq']."' ";
		if ($_POST['ch_pwd'] != "") $update .= ", `heslo` = '".md5($_POST['ch_pwd'])."' ";
		if (MaPrava ("hlidka") ) {
			$update .= ", `hlidka_pwr_abs` = '".$_POST['ch_hlidka_abs']."'
							, `hlidka_pwr_rel` = '".$_POST['ch_hlidka_rel']."'
							, `hlidka_pwr_need_both` = '".($_POST['ch_hlidka_both'] == "on" ? "1" : "0")."'
							, `hlidka_mail` = '".$_POST['ch_hlidka_mail']."'
							, `hlidka_phone` = '".$_POST['ch_hlidka_phone']."'
							, `vzdy_prozvonit` = '".($_POST['ch_vzdy_prozvonit'] == "on" ? "1" : "0")."'
							, `custom_hlidka_msg` = '".$_POST['ch_custom_hlidka_msg']."'";
		}
		$update .= "WHERE `ID` = '".$user_info['ID']."'";
		if (MySQL_Query ($update)) {
			echo "�daje zm�n�ny.";
			if ($_POST['ch_pwd'] != "") Prihlas ($user_info['login'], md5($_POST['ch_pwd']));
			NactiUdajeOUserovi();
		} else {
			echo "Nastala z�va�n� chyba.<br>";
		}
	}
}
echo '
<form action="main.php" method="post">
<input type="hidden" name="typ" value="zmen_usera">
<table>
<tr>
	<td>
		Login:
	</td>
	<td>
		'.htmlspecialchars($user_info['login']).'
	</td>
</tr>
<tr>
<td>
		Regent:
	</td>
	<td>
		'.htmlspecialchars($user_info['regent']).'
	</td>
</tr>
<tr>
	<td>
		Provincie:
	</td>
	<td>
		'.htmlspecialchars($user_info['provi']).'
	</td>
</tr>
<tr>
	<td>
		ICQ <i>(bez mezer)</i>:
	</td>
	<td>
		<input name="ch_icq" value="'.($user_info['icq'] > 0 ? $user_info['icq'] : '').'">
	</td>
</tr>
<tr>
	<td>
		Popis:
	</td>
	<td>
		<input name="ch_popis" value="'.htmlspecialchars($user_info['popis']).'">
	</td>
</tr>
<tr>
	<td>
		Zm�na hesla:
	</td>
	<td>
		<input name="ch_pwd" type="password">
	</td>
</tr>
<tr>
	<td>
		Heslo znovu:
	</td>
	<td>
		<input name="ch_pwd2" type="password">
	</td>
</tr>
<tr>
	<td>
		Anal�z aukce:
	</td>
	<td>
		'.$user_info['aukce_count'].'
	</td>
</tr>
<tr>
	<td>
		Simulac� boje:
	</td>
	<td>
		'.$user_info['simul_count'].'
	</td>
</tr>
<tr>
	<td>
		Chat:
	</td>
	<td>
		'.$user_info['chat_count'].'
	</td>
</tr>';

if (MaPrava ("hlidka") ) {
	echo '
	<tr>
		<td>Pwr hranice abs <i>(cista sila)</i>:</td>
		<td><input name="ch_hlidka_abs" value="'.$user_info['hlidka_pwr_abs'].'" style="text-align: right;"></td>
	</tr>
	<tr>
		<td>Pwr hranice rel <i>(procent sily, cele cislo)</i>:</td>
		<td><input name="ch_hlidka_rel" value="'.$user_info['hlidka_pwr_rel'].'" style="text-align: right;"> %</td>
	</tr>
	<tr>
		<td>Mus� platit oboje z�rove�:</td>
		<td><input type="checkbox" name="ch_hlidka_both"'.($user_info['hlidka_pwr_need_both'] == 1 ? ' checked' : '').'></td>
	</tr>
	<tr>
		<td>Mail:</td>
		<td><input name="ch_hlidka_mail" value="'.htmlspecialchars($user_info['hlidka_mail']).'" style="text-align: right;"></td>
	</tr>
	<tr>
		<td>Telefon:</td>
		<td><input name="ch_hlidka_phone" value="'.htmlspecialchars($user_info['hlidka_phone']).'" style="text-align: right;"></td>
	</tr>
	<tr>
		<td>V�DY prozv�n�t:</td>
		<td><input type="checkbox" name="ch_vzdy_prozvonit"'.($user_info['vzdy_prozvonit'] == 1 ? ' checked' : '').'></td>
	</tr>
	<tr>
		<td>Vlastn� zpr�va (<a href="./hlidka/custom_msg_help.php" target="_blank" class="other">help</a>):</td>
		<td><input name="ch_custom_hlidka_msg" value="'.$user_info['custom_hlidka_msg'].'"></td>
	</tr>
	
	';	
}

echo '<tr>
	<td>
		&nbsp;
	</td>
	<td>
		<input type="submit" value="Ulo� zm�ny">
	</td>
</tr>
</table>
</form>';
?>