<?php
{OBECNE_INFO} = '
	<div class="title">
		Informace
	</div> <!-- //title -->
	<table>
	<tr>
		<td>V�k:</td>
		<td>
			5.1 (sekera je 2 zl)
		</td>
	</tr>
	<tr>
		<td>
			Autor
		</td>
		<td>
			Anonymn�
		</td>
	</tr>
	<tr>
		<td>
			OT
		</td>
		<td>
			1 183
		</td>
	</tr>
	<tr>
		<td>
			S�la
		</td>
		<td>
			220 254
		</td>
	</tr>
	<tr>
		<td>
			P�esv�d�en�
		</td>
		<td>Dobr�
		</td>
	</tr>
	<tr>
		<td>
			Povol�n�
		</td>
		<td>
			Barbar
		</td>
	</tr>
	<tr>
		<td>
			Br�na
		</td>
		<td>
			Br5na
		</td>
	</tr>
	<tr>
		<td>
			V�sledek
		</td>
		<td class="vyhral">
			Va�e ztr�ty jsou 12.93 %, obr�nce 41.29 %.
		</td>
	</tr>
	</table>';
{KOLO_1..4} = '<div class="kolo"><div class="utok">
			490 x Erl�v mu�ket�r -&gt; 16365 x Valk�ra<br />zabito: 191, zb�v�: 16174 / 16365 zran�no: 1 %<br />brn�n� zb�v�: 96
%<br /></div>
			<div class="utok">
			90 x Erl�v revenant -&gt; 16174 x Valk�ra<br />zabito: 186, zb�v�: 15988 / 16365 zran�no: 1 %<br />brn�n� zb�v�: 94
%<br /></div>
			<div class="obrana">
			120 x Ledov� dr��ek =&gt; 440 x Erl�v ��dov� ryt��<br />zabito: 3, zb�v�: 437 / 440 zran�no: 1 %<br />brn�n� zb�v�: 97
%<br /></div>
			</div> <!-- //kolo -->';
{HOSP_PRED..PO} = '<table>
		<tr>
			<td>
				<strong>Jednotka</strong>
			</td>
			<td>
				<strong>XP</strong>
			</td>
			<td>
				<strong>Typ</strong>
			</td>
			<td>
				<strong>S�la</strong>
			</td>
			<td>
				<strong>Po�et</strong>
			</td>
			<td>
				<strong>zl/TU</strong>
			</td>
			<td>
				<strong>mn/TU</strong>
			</td>
			<td>
				<strong>pp/TU</strong>
			</td>
		</tr>			
		
		<tr>
			<td>
				Valk�ra
			</td>
			<td class="right">
				71.59
			</td>
			<td>
				LB2
			</td>
			<td class="right">
				209 191
			</td>
			<td class="right">
				15 795
			</td>
			<td class="right">
				-10 267
			</td>
			<td class="right">
				-18 954
			</td>
			<td class="right">
				0
			</td>
		</tr>
		
		
		<tr>
			<td>
				Ledov� dr��ek
			</td>
			<td class="right">
				100.00
			</td>
			<td>
				LS
			</td>
			<td class="right">
				10 717
			</td>
			<td class="right">
				120
			</td>
			<td class="right">
				-655
			</td>
			<td class="right">
				-874
			</td>
			<td class="right">
				0
			</td>
		</tr>
		
		
		<tr>
			<td>
				Kyklop
			</td>
			<td class="right">
				26.71
			</td>
			<td>
				PB1
			</td>
			<td class="right">
				108
			</td>
			<td class="right">
				2
			</td>
			<td class="right">
				-42
			</td>
			<td class="right">
				-39
			</td>
			<td class="right">
				0
			</td>
		</tr>
		
		
		<tr>
			<td>
				Pol�rn� bou�e
			</td>
			<td class="right">
				44.17
			</td>
			<td>
				LB1
			</td>
			<td class="right">
				96
			</td>
			<td class="right">
				1
			</td>
			<td class="right">
				-12
			</td>
			<td class="right">
				-18
			</td>
			<td class="right">
				-2
			</td>
		</tr>
		
		
		<tr>
			<td>
				Dra�� d�mon
			</td>
			<td class="right">
				39.38
			</td>
			<td>
				LB2
			</td>
			<td class="right">
				86
			</td>
			<td class="right">
				1
			</td>
			<td class="right">
				-25
			</td>
			<td class="right">
				-21
			</td>
			<td class="right">
				-3
			</td>
		</tr>
		
		
		<tr>
			<td>
				Apo�tol
			</td>
			<td class="right">
				23.80
			</td>
			<td>
				LB1
			</td>
			<td class="right">
				56
			</td>
			<td class="right">
				11
			</td>
			<td class="right">
				-10
			</td>
			<td class="right">
				-46
			</td>
			<td class="right">
				0
			</td>
		</tr>
		
		
		<tr>
			<td colspan="8">
				<hr>
			</td>
		</tr>
		<tr>					
			<td colspan="5">
				Stavby a budovy
			</td>				
			<td class="right">
				123
			</td>
			<td class="right">
				456
			</td>
			<td class="right">
				789
			</td>
		</tr>
		<tr>					
			<td colspan="5">
				Podann�
			</td>				
			<td class="right">
				101 112
			</td>
			<td class="right">
				131 415
			</td>
			<td class="right">
				161 718
			</td>
		</tr>
		<tr>					
			<td colspan="5">
				Arm�da
			</td>				
			<td class="right">
				-11 011
			</td>
			<td class="right">
				-19 952
			</td>
			<td class="right">
				-5
			</td>
		</tr>
		<tr>					
			<td colspan="8">
				<hr>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				Celkem
			</td>		
			<td class="right">
				90 224
			</td>
			<td class="right">
				111 919
			</td>
			<td class="right">
				162 502
			</td>
		</tr>
	
</table>';
?>