<?php
/* overeni uzivatele */
require_once ("fce.php");

if (!CheckLogin () || !MaPrava("simul")) {
  LogOut();
}
/* ------ */

$newDesign = true;
$newDesII = true;


echo "<center><hr>";
for ($kolo=1;$kolo<=4;$kolo++)
{  
  echo "Round $kolo<br>\n<br>\n";
  
  if ($newDesign) echo '<table class="simulNewDes">';
  
  NastavStartKola();
  
  if ($ut[$kolo] != 0) kouzli (1,$ut[$kolo]);
  if ($ob[$kolo] != 0) kouzli (2,$ob[$kolo]);
  
  SeradDleIni($kolo);
  
  if ($ini != "")
  {
    foreach ($ini as $utok => $value)
    {
        if (($vojak[$utok]->pocet != 0)&&($vojak[$utok]->jmeno != ""))
          {
            if ($newDesign) echo "<tr><td>";
            echo '<div class="'.($vojak[$utok]->hrac == 1?"utok":"obrana").'">';
            echo $vojak[$utok]->pocet." x ".VojakSLinkem($vojak[$utok])." (ini ".round($value).") ".($vojak[$utok]->hrac == 1?"=":"-")."> ";
            $cil = ZjistiVhodnyCil($vojak[$utok]->hrac,(($vojak[$utok]->typ=="S")||($vojak[$utok]->druh=="L"))?1:0, $vojak[$utok]->utok_na);
            if ($cil > -1)
            {
              echo $vojak[$cil]->pocet."/".$vojak[$cil]->pocet_max." x ".VojakSLinkem($vojak[$cil])." (CS zb�v�: ".max(0,$vojak[$cil]->cs-($vojak[$utok]->typ=="B"?1:0)).")<br>\n";
              
              $dmg = $vojak[$utok]->getDamage(); 
              
              $mod = ZjistiBonusZaTyp($vojak[$utok],$vojak[$cil]);
              $dmg *= $mod;
              if ($SHOW_STATS == 1) echo "<tr><td>Modifikator za typ jednotek $mod</td><td>dam: ".cislo($dmg)."<br>\n";
              
              /*$dobrneni = min($dmg*$ARMOR_ABSORB,$vojak[$cil]->brn_zbyva*$vojak[$cil]->pocet*$vojak[$cil]->xp/100); //*$vojak[$cil]->brn_zbyva/$vojak[$cil]->brn
              $dmg -= $dobrneni;
              $vojak[$cil]->brn_zbyva -= ($dobrneni/$vojak[$cil]->pocet/$vojak[$cil]->xp*100);
              if ($SHOW_STATS == 1) echo "<tr><td>Brneni vzalo ".cislo($dobrneni)."</td><td>dam: ".cislo($dmg)."<br>\n";
              if ($SHOW_STATS == 1) echo "<tr><td></td><td>Brn zbyva ".cislo($vojak[$cil]->brn_zbyva)."</td></tr>";
         
              $dorazeno = min ($vojak[$cil]->pocetZranenych, floor ($dmg / ($vojak[$cil]->zivotyZraneneJednotky()*$vojak[$cil]->xp/100 * $vojak[$cil]->hlas_krve)));
              $dmg -= $dorazeno * $vojak[$cil]->zivotyZraneneJednotky()*$vojak[$cil]->xp/100 * $vojak[$cil]->hlas_krve; 
              if ($SHOW_STATS == 1) echo "<tr><td>Dora�eno jednotek ".cislo($dorazeno)."</td><td>dam: ".cislo($dmg)."</td></tr>";
              
              $dmg_na_zranene = round ($dmg * CAST_ZRAN); //kolik dmg pujde na zranovani
              $dmg -= $dmg_na_zranene;
              if ($SHOW_STATS == 1) echo "<tr><td>Dmg na zranene ".cislo($dmg_na_zranene)."</td><td>dam: ".cislo($dmg)."</td></tr>";
               
              $killed = min ($vojak[$cil]->pocet - $dorazeno, floor ($dmg / ($vojak[$cil]->zvt*$vojak[$cil]->xp/100 * $vojak[$cil]->hlas_krve)));
              $zraneno = min ($vojak[$cil]->pocet - $dorazeno - $killed, floor ($dmg_na_zranene / ($vojak[$cil]->zvt*$vojak[$cil]->xp/100 * $vojak[$cil]->hlas_krve)));
                            
              $vojak[$cil]->pocetZranenych += $zraneno;
              
              if ($SHOW_STATS == 1) echo "<tr><td>Zran�no jednotek ".cislo($zraneno)."</td><td></td></tr>
                                          <tr><td>Zran�no celkem ".cislo($vojak[$cil]->pocetZranenych - $dorazeno)."</td><td></td></tr>";*/

              $umrti = $vojak[$cil]->receiveDamage ($dmg);
              $killed = $umrti['killed'];
              $dorazeno = $umrti['dorazeno'];
              $zraneno = $umrti['zraneno'];
             
              if ($SHOW_STATS == 1) echo "</table>";
             
              echo "Zabito: ".($killed + $dorazeno).", zb�v�: ".($vojak[$cil]->pocet - $killed - $dorazeno)."/".$vojak[$cil]->pocet_max.", zran�no: ".($vojak[$cil]->pocetZranenychPercent($killed, $dorazeno) * 100)."% (".($vojak[$cil]->pocetZranenych - $dorazeno).")<br>\n";
              if ($vojak[$cil]->brn == 0) $zbyva = 0; else $zbyva = max($vojak[$cil]->brn_zbyva/$vojak[$cil]->brn*100, 0);
              
              echo "Brn�n� zb�v�: ".cislo($zbyva)."%".($newDesign ? '</td><td>'.graf(($vojak[$cil]->pocet - $killed)/$vojak[$cil]->pocet_max, $newDesII ? $vojak[$cil]->pocet/$vojak[$cil]->pocet_max : 0).'</td></tr><tr><td>' : '<br>')."\n";
                //------- CSko ---------
              if (($vojak[$utok]->typ == "B")&&($vojak[$cil]->cs > 0))
              {
                $vojak[$cil]->cs --;
                if ($newDesign) echo "</td></tr><tr><td>";
                echo "<div class=\"CS\"><br>Obrana:<br />\n".$vojak[$cil]->pocet." x ".VojakSLinkem($vojak[$cil])." ".($vojak[$utok]->hrac == 1?"-":"=")."> ";
                echo $vojak[$utok]->pocet."/".$vojak[$utok]->pocet_max." x ".VojakSLinkem($vojak[$utok])." <br />\n";
                
                $dmg = $vojak[$cil]->getDamage();
                
                $mod = ZjistiBonusZaTyp($vojak[$utok],$vojak[$cil], true);
                $dmg *= $mod;
                if ($SHOW_STATS == 1) echo "<tr><td>Modifikator za typ jednotek $mod</td><td>dam: ".cislo($dmg)."<br>\n";
                
                $umrti = $vojak[$utok]->receiveDamage ($dmg);
                $killed_cs = $umrti['killed'];
                $dorazeno_cs = $umrti['dorazeno'];
                $zraneno_cs = $umrti['zraneno'];
               
                             
                if ($SHOW_STATS == 1) echo "</table>";
               
                echo "Zabito: ".($killed_cs + $dorazeno_cs).", zb�v�: ".($vojak[$utok]->pocet - $killed_cs - $dorazeno_cs)."/".$vojak[$utok]->pocet_max.", zran�no: ".($vojak[$utok]->pocetZranenychPercent($killed_cs, $dorazeno_cs) * 100)."% (".($vojak[$utok]->pocetZranenych - $dorazeno_cs).")<br>\n";
                if ($vojak[$utok]->brn == 0) $zbyva = 0; else $zbyva = max(0,$vojak[$utok]->brn_zbyva/$vojak[$utok]->brn*100);
                echo "Brn�n� zb�v�: ".cislo($zbyva)."%".($newDesign ? '</td><td>'.graf(($vojak[$utok]->pocet-$killed_cs)/$vojak[$utok]->pocet_max, $newDesII ? $vojak[$utok]->pocet/$vojak[$utok]->pocet_max : 0).'</td></tr><tr><td>' : '<br>')."\n";
                $vojak[$utok]->pocet -= $killed_cs + $dorazeno_cs;
                $vojak[$utok]->pocetZranenych -= $dorazeno_cs;
                echo "</div>";
              }
              $vojak[$cil]->pocet -= $killed + $dorazeno;
              $vojak[$cil]->pocetZranenych -= $dorazeno;
            }
            else
            {
                echo "Nema cil<br>\n";
            }
            echo "<br>\n";
            echo "</div>\n";
            if ($newDesign) echo "</td></tr>";
          }
    }
  }
  else
  {
    echo "��dn� bojov� jednotky pro toto kolo!<br>";
  }
  if ($newDesign) echo "</table>";
  echo "<hr>";
   
}
?>
