<?php
/**
 * Knihovni�ka na neposedn� zra�ov�n� :-)
 *  
 * @author Savannah <savannah@seznam.cz>
 * @version 1.0
 * @package Simul
 */
/**
 * T��da zran�n�. 
 * M�la by po��tat v�echny aproxima�n� funkce v boji kter�
 * jsem jako kokot po��tal, tak douf�m, �e to bude fungovat
 * @author Savannah <savannah@seznam.cz>
 * @version 1.0
 * @package Simul
 */ 
  class Zraneni {
    
    /**
     * Konstruktor pr�zdn�
     */
    function Zraneni () {
      ;
    }
    
    /**
     * Usm�rn�n� zabit�ch    
     *      
     * Zjist� kolik jednotek m� b�t zabito v z�vislosti na po�tu zran�n�ch p�ed ranou a po�tu zabit�ch dle simulu.  <br />
     * Pou�it� - v�dy.<br />
     * Source - zabito_simul_real.xls->"LN vs LOG"      
     * @param integer $zranenoPredN po�et zran�n�ch p�ed [N]
     * @param integer $zabitoSimulN po�et mrtv�ch dle simulu [N]
     * @return integer jak� ��st z $zabitoSimulN m� zem��t [%] 
     */            
    function atv ($zranenoPredN, $zabitoSimulN) {
      if ($zabitoSimulN == 0) return 0;
      
      $T = exp(1)/3;
      $U = 2.1540;
      $V = 42.165;
      $W = 20; 
      
      return min(
        (
          pow($zranenoPredN/$zabitoSimulN,$T)
            +$U
        )*$V/200        
        ,$W);
    }
    
    /**
     * Usm�rn�n� zran�n�ch (nezran�n�/m�lo zran�n�)
     *      
     * Zjist� kolik jednotek bude zran�no po r�n�.<br />
     * Pou�it� - pro jednotky zat�m nezran�n�/m�lo zran�n�.<br />
     * Source - zra(1).xls->List1
     * @param double $sila pom�r jednotek zabit�ch (real, [N]) a zbyl�ch P�ED ranou([N]) 
     * @return double absolutn� mno�stv� zran�n�ch po r�n� ([%]) 
     */           
    function vyrobZranene1 ($sila) {
      if ($sila > 1) $sila = 1;
      if ($sila < 0) $sila = 0;
      return (0.60*pow($sila,3) - 1.38*pow($sila,2) + 1.39*$sila);
    }
  
     /**
     * Usm�rn�n� zran�n�ch (v�ce zran�n�)    
     *      
     * Zjist� kolik jednotek bude zran�no po r�n�.<br />
     * Pozn.: po�et zran�n�ch by nem�l p�ekro�it 60%! Tam u� se bude asi jednat o chybu randomu nebo tak, jinak to vych�z� kr�sn�.<br />
     * Pou�it� - zran�no p�ed >= 3%.<br />
     * Source - odchylka_dorazeni.xls->"k=0, dalsi" a odchylka2.xls
     * @param double $zabito po�et zabit�ch ranou (po �prav�ch, zabito real, [N])
     * @param double $zranenoPred po�et zran�n�ch p�ed ranou ([N])
     * @return double relativn� mno�stv� zran�n�ch po r�n� (tzn. t�m vyn�sobim po�et zran�nc�h, [%])
     */       
    function vyrobZranene2 ($zabito, $zranenoPred) {
      if ($zranenoPred == 0) {
        trigger_error("0 zran�n�ch do fce vyrobZranene2!", E_USER_WARNING);
        return 0;
      }
      // prvni aproximacni prodlouzeni, "prava" cast grafu
      if ($zabito/$zranenoPred > 0.3) {
        return 1/(0.7353*pow($zabito/$zranenoPred,-0.8551));
      } else {
        return exp(-2.1506*$zabito/$zranenoPred);
      }
    }
  }
?>
